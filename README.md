# Blotto #

This code was written in order to solve a game-puzzle I was set during a job application to a large financial trading firm based in London. 
It is a fun game, where you have to come up with the best solution. Full explanation of the rules, and how I went about playing, are in blotto-writeup.pdf.

### Who do I talk to? ###

Timothy Burd (timothy.burd@chem.ox.ac.uk / timothyahburd@googlemail.com) would be excited to hear about anyone using this code for whatever purpose, and is happy to answer any and all questions.


