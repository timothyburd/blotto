import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv
import string

def generate_random_solution(n_soldiers, n_castles):
    """
    Produce a random solution based on a uniform distribution of soldiers
    """

    solution = [0 for i in range(n_castles)]
    for i in range(n_soldiers):
        castle = random.randint(0, n_castles-1)
        solution[castle] += 1
    return solution


def generate_random_weighted_solution(n_soldiers, n_castles):
    """
    Produce a random solution based on a linear distribution of soldiers
    """

    castle_list = range(n_castles)
    solution = [0 for i in castle_list]
    linear_weighting =[i / sum(castle_list) for i in castle_list]

    for i in range(n_soldiers):
        castle = np.random.choice(castle_list, 1,p=linear_weighting)[0]
        solution[castle] += 1
    return solution


def generate_random_log_weighted_solution(n_soldiers, n_castles):
    """
    Produce a random solution based on a log distribution of soldiers
    """

    castle_list = range(n_castles)
    solution = [0 for i in castle_list]
    log_weighting =[np.log(i+2)  for i in castle_list]
    log_weighting = [i / sum(log_weighting) for i in log_weighting]

    for _ in range(n_soldiers):
        castle = np.random.choice(castle_list, 1,p=log_weighting)[0]
        solution[castle] += 1
    return solution


def generate_uniform_solution(n_soldiers, n_castles):
    """
    Produce a random solution based on a uniform distribution of soldiers
    """

    solution = [n_soldiers / n_castles for i in range(n_castles)]
    return solution

def generate_linear_solution(n_soldiers, n_castles):
    """
    Produce a random solution based on a linear distribution of soldiers
    """
    point_list = range(1, n_castles+1)
    solution = [n_soldiers * points /sum(point_list) for points in point_list] # linear distribution of soldiers

    solution = [round(i) for i in solution]
    excess_soldiers = sum(solution) - n_soldiers


    # allocate excess soldiers randomly for now
    if excess_soldiers > 0:
        for i in range(excess_soldiers):
            castle_number = random.randint(1, n_castles+1)
            solution[castle_number] -=1
    if excess_soldiers < 0:
        for i in range(excess_soldiers):
            castle_number = random.randint(1, n_castles+1)
            solution[castle_number] +=1

    return solution


def compete_random_tournament(solution, n_players):
    """
    Organise a tournament with n_players against your solution.
    Compete against three stratagies - uniform, linear and log distributions
    :return: Win percentage
    """

    n_soldiers = int(sum(solution))
    n_castles = int(len(solution))
    wins = 0

    trials_per_type = int(n_players/3)

    for _ in range(trials_per_type):
        random_solution = generate_random_weighted_solution(n_soldiers, n_castles)
        wins += find_winner(solution, random_solution)
    for _ in range(trials_per_type):
        random_solution = generate_random_solution(n_soldiers, n_castles)
        wins += find_winner(solution, random_solution)
    for _ in range(trials_per_type):
        random_solution = generate_random_log_weighted_solution(n_soldiers, n_castles)
        wins += find_winner(solution, random_solution)

    win_percentage = wins / (3 * trials_per_type)
    return win_percentage

def find_winner(solution_A, solution_B):
    """
    Find the winner out of two solutions. Return 1 for A, return 0 for B
    """
    point_list = range(1, len(solution_A)+1)
    points_A = 0
    points_B = 0
    winning_score = 20

    for A,B, points in zip(solution_A, solution_B, point_list):
        if A > B:
            points_A += points
        if A < B:
            points_B += points
        else:
            pass
        if points_A >= winning_score:
            return points_A
            return 1
        if points_B >= winning_score:
            return points_A
            return 0
    return points_A



def monte_carlo_step(input_answer):
    """
    Make one change to a random soldier's position - other MC steps could be used too!
    """
    new_answer = input_answer.copy()

    # select random element to go up
    castle_to_go_up = random.randint(0,len(input_answer)-1)

    # select random element to go down
    castle_to_go_down = random.randint(0,len(input_answer)-1)
    while input_answer[castle_to_go_down] < 1:  # cannot take a soldier out of an empty castle
        castle_to_go_down = random.randint(0, len(input_answer) - 1)

    new_answer[castle_to_go_up] += 1
    new_answer[castle_to_go_down] -= 1

    return new_answer

def plot_score_over_time(name_list):


    df = pd.read_csv(name_list[0])

    for letter in name_list[1:]:


        b = pd.read_csv(letter)
        df = pd.concat([df, b], axis=0)
    # df = df.reset_index()
    df['trial_number'] = np.arange(len(df))

    df['average_score'] = (1 / 3) * (df['score1'] + df['score2'] + df['score3'])
    plt.plot(df['trial_number'], df['average_score'])
    plt.ylabel(("Average Score"))
    plt.xlabel("Step Number")
    plt.savefig("av_mc.pdf")
    plt.show()





def perform_mc_simulation(input_answer, n_trials, temperature, n_steps):
    """
    Perform a Monte Carlo simulation through castle-soldier space, starting with input_answer configuration
    """

    target = 55
    random_key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))   # Random key for identifying different runs

    f = open("mc_" + random_key + ".csv", 'w')
    f.write("trial,a,b,c,d,e,f,g,h,i,j,score1,score2,score3,n_trials,temperature\n")

    old_state = input_answer.copy()

    for trial in range(n_steps):

        if trial % 50 == 0:
            temperature *= 0.5      # Decrease temperature as simulation goes on

        # Simulate a few tournaments using our trial configuration. How well does it do?
        score_old_1 = compete_random_tournament(old_state, n_trials)
        score_old_repeat = compete_random_tournament(old_state, n_trials)
        score_old_repeat_2 = compete_random_tournament(old_state, n_trials)

        score_old = (1/3) * (score_old_1 + score_old_repeat+ score_old_repeat_2)

        # Make a random move from our trial configuration
        new_state = monte_carlo_step(old_state)

        # Simulate a tournament using our new trial configuration. How well does it do?
        score_new = compete_random_tournament(new_state, n_trials)

        if score_new >= score_old:      # New configuration is better, accept the move
            old_state = new_state
        if score_new < score_old:       # New configuration is worse. Do a Metropolis step to see if accepted.
            weight = np.exp((score_new - score_old)/temperature)        # this is between 0 and 1
            dice = random.uniform(0, 1)

            if weight > dice:   # Accept the move
                old_state = new_state

            else:               # Reject the move
                old_state = old_state
                pass

        # Keep a mode of the configurations we trial
        f.write(str(trial) + ",")
        for r in old_state:
            f.write(str(r) + ",")
        f.write(str(score_old) + "," + str(score_old_repeat) + "," + str(score_old_repeat_2) + "," + str(n_trials) + "," + str(temperature) +  "\n")
        print("Trial: ", trial, ", Configuration: ", old_state, ", Score: ", score_old, ", n_trials: ", n_trials, ", T = ", temperature)

        # Provide option to finish the simulaion if you surpass a certain ideal score
        if score_old > target:
            break

        # How many trials in the tournament we need to do depends on how well we are doing. Good scoring configurations need lots of trials to accurately determine theur score!
        if score_old < 20:
            n_trials = 20
        elif score_old < 23:
            n_trials = 100
        elif score_old < 25:
            n_trials = 400
        else:
            n_trials = 1000

    print(old_state)        # Print the final state in the simulation
    f.close()
    return old_state, random_key

def main():
    # Game setup
    n_soldiers = 100
    n_castles = 10

    # Simulation setup
    temperature = 10    # Starting temperature
    n_trials = 20       # number of players in the tournaments initially
    n_steps = 600       # Number of MC steps in the simulation
    n_simulations = 5

    # Initial configuration
    input_answer = generate_uniform_solution(n_soldiers, n_castles)

    # Run simulation
    for _ in range(n_simulations):
        answer, key = perform_mc_simulation(input_answer, n_trials, temperature, n_steps)
        input_answer = answer.copy()
        plot_score_over_time(["mc_" + key + ".csv"])


if __name__ == '__main__':
    main()
